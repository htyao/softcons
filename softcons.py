import sys

import numpy as np
from Bio import SeqIO

import RNA


def read_alignment(fpath, ali_format='fasta'):
    """Read alignment in stockholm format
    """
    with open(fpath) as f:
        for record in SeqIO.parse(f, ali_format):
            seq = str(record.seq)
            seq = seq.upper().replace("T", "U")
            yield seq


class Alignment:
    def __init__(self, alignment, md=RNA.md()):
        """Create alignment object from a given aligned file
        """
        self.md = md
        self.aligned = [seq for seq in alignment]
        self.ungapped = [seq.replace('-','') for seq in self.aligned]
        self.consensus = ""
        self.fc = None
        self.ps_energy = []      # 1-indexed
        self.dominant_bps = []   # 0-indexed


        self._fold()

    @classmethod
    def from_file(cls, filepath, md=RNA.md(), ali_format='fasta'):
        return cls(read_alignment(fpath, ali_format), md)



    def _fold(self):
        """Get consensus structure and bpp
        """
        fc = RNA.fold_compound(self.aligned, self.md)
        ss, mfe = fc.mfe()
        fc.exp_params_rescale(mfe)
        fc.pf()

        self.consenssus = ss
        self.fc = fc
        self.ps_energy = self._pseudo_energy()
        self.dominant_bps = [(ai-1, aj-1) for ai, aj in zip(*np.where(self.ps_energy<0))]


    def _pseudo_energy(self):
        """Create pseudo energy metrix
        """
        RT = RNA.GASCONST*(self.md.temperature + RNA.K0)/1000
        bpp = np.array(self.fc.bpp())
        paired = bpp==1
        unpaired = bpp==0
        bpp[paired] = np.NaN
        bpp[unpaired] = np.NaN

        tmp = np.minimum(0, -RT*np.log(bpp/(1-bpp)))
        tmp[paired] = -999
        tmp[unpaired] = 0
        return tmp


    def get_energy_of_aligned_seq(self, aligned):
        """Return pseudo energy matrix for aligned sequence
        """

        ungap = np.array(['X']+list(aligned)) != '-'
        l = np.sum(ungap)
        ungap_bps = np.outer(ungap, ungap)
        res = self.ps_energy.copy()
        return res[ungap_bps].reshape((l,l))


    def add_sc(self, fc, aligned):
        """Add soft constraint to fc
        """
        to_add = self.get_energy_of_aligned_seq(aligned)

        fc.sc_add_bp(to_add.tolist())


    def fc_of_aligned_seq(self, aligned):
        """Create fold compound for given aligned sequence with soft constraint
        """
        seq = aligned.replace('-', '')
        fc = RNA.fold_compound(seq)
        self.add_sc(fc, aligned)
        return fc


if __name__ == "__main__":
    fpath = sys.argv[1]
    alignment = read_alignment(fpath)
    target_seq_aligned = next(alignment)
    target_seq = target_seq_aligned.replace('-', '')
    print(target_seq)
    alignment =  Alignment(alignment)

    fc = alignment.fc_of_aligned_seq(target_seq_aligned)

    print(target_seq)
    print("MFE")
    print(RNA.fold(target_seq)[0])
    print("Use consensus information")
    print(fc.mfe()[0])

