Packages viennarna, numpy, and biopython are required to run the code. There's a very simple example code at the bottom of py file. Use follow command to run it

```
python softcons.py RF00010.afa.txt
```

Several warnings are returned by viennarna due to the use of sc_add_bp(matrix). This will be fixed in the future once Ronny accepts my merge request.
